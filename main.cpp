// Stdlib Headers
#include <stdio.h>

// SFML Headers
#include <SFML/Graphics.hpp>

// Utility Headers
#include "TimeScaling.h"

// Game Headers
#include "Game.h"
#include "Player.h"

// Constants
#define TARGET_FRAMERATE 60//FPS

/// Entry point of application
///
/// \return Application exit code
///
int main()
{

    // Create the main window
    sf::RenderWindow App(sf::VideoMode(800, 600), "SFML Smooth Animation");

    // Create the game's controller
    Game game = Game(&App);

    Player player = Player(&App);

    // Initialize the animating smothing system
    TimeScaling scaling = TimeScaling(TARGET_FRAMERATE);
    float timeScale;

    // Enable Vsync (Make an option later.)
    App.UseVerticalSync(true);
    //App.SetFramerateLimit(10);

    // Start the game loop
    while (App.IsOpened())
    {
        // Get the time scale value
        timeScale = scaling.getTimeScale();

        // Process events
        sf::Event Event;
        while (App.GetEvent(Event))
        {
            // Close window : exit
            if (Event.Type == sf::Event::Closed)
                App.Close();

            // Escape key : exit
            if (Event.Type == sf::Event::KeyPressed)
            {
                if (Event.Key.Code == sf::Key::Escape)
                {
                    App.Close();
                }
                if (Event.Key.Code == sf::Key::Up || Event.Key.Code == sf::Key::Space)
                {
                    if (!player.Jumping())
                    {
                        player.SetJumping(true);
                    }
                }
                if (Event.Key.Code == sf::Key::Left)
                {
                    if (!player.MovingLeft())
                    {
                        player.SetMovingLeft(true);
                    }

                }
                if (Event.Key.Code == sf::Key::Right)
                {
                    if (!player.MovingRight())
                    {
                        player.SetMovingRight(true);
                    }
                }
            }

            if (Event.Type == sf::Event::KeyReleased)
            {
                if (Event.Key.Code == sf::Key::Up || Event.Key.Code == sf::Key::Space)
                {
                    player.SetJumping(false);
                }
                if (Event.Key.Code == sf::Key::Left)
                {
                    player.SetMovingLeft(false);
                }
                if (Event.Key.Code == sf::Key::Right)
                {
                    player.SetMovingRight(false);
                }
            }
        }

        // Clear screen
        App.Clear();

        game.Update(timeScale);
        player.Update(timeScale);

        game.Draw();
        player.Draw();

        // Finally, display the rendered frame on screen
        App.Display();
    }

    return EXIT_SUCCESS;
}
