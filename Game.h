#ifndef GAME_H
#define GAME_H
#include <SFML/Graphics.hpp>

class Game
{
    public:
        Game(sf::RenderWindow * Application);
        virtual ~Game();
        void Update(float timeScale);
        void Draw();
    protected:
    private:
        sf::RenderWindow * App;
        sf::Shape CIRCLEMAN;
        int CIRCLEMAN_X;
        int CIRCLEMAN_Y;
        sf::Shape floor;
};

#endif // GAME_H
