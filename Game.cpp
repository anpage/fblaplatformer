#include "Game.h"

#define MOVEMENT 10

Game::Game(sf::RenderWindow * Application) {
    App = Application;
    CIRCLEMAN = sf::Shape::Circle(0, 0, 20, sf::Color(255, 0, 0));
    CIRCLEMAN.SetPosition(20, 20);
    CIRCLEMAN_X = MOVEMENT;
    CIRCLEMAN_Y = MOVEMENT+(MOVEMENT/2);
    floor = sf::Shape::Rectangle(0, App->GetHeight()-50, App->GetWidth(), App->GetHeight(), sf::Color(0, 128, 255));
}

Game::~Game() {
    //dtor
}

void Game::Update(float timeScale) {
    CIRCLEMAN.Move(CIRCLEMAN_X*timeScale,CIRCLEMAN_Y*timeScale);
    if (CIRCLEMAN.GetPosition().x < 20)
    {
        CIRCLEMAN_X = MOVEMENT;
    }
    if (CIRCLEMAN.GetPosition().x > App->GetWidth() - 20)
    {
        CIRCLEMAN_X = -MOVEMENT;
    }
    if (CIRCLEMAN.GetPosition().y < 20)
    {
        CIRCLEMAN_Y = MOVEMENT+(MOVEMENT/2);
    }
    if (CIRCLEMAN.GetPosition().y > (App->GetHeight() - 70))
    {
        CIRCLEMAN_Y = -MOVEMENT-(MOVEMENT/2);
    }
}

void Game::Draw() {
    App->Draw(CIRCLEMAN);
    App->Draw(floor);
}
