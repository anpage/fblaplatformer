#include "TimeScaling.h"

TimeScaling::TimeScaling(float targetFPS)
{
    this->targetFPS = targetFPS;
    QueryPerformanceCounter(&lastFrame);
    QueryPerformanceFrequency(&ticksPerSecond);
}

TimeScaling::~TimeScaling()
{
    //dtor
}

float TimeScaling::getTimeScale()
{
    QueryPerformanceCounter(&currentTicks);
    float timeScale = (float)(currentTicks.QuadPart-lastFrame.QuadPart)/((float)ticksPerSecond.QuadPart/targetFPS);
    if (timeScale <= 0.0d)
        timeScale = 1.0d;
    lastFrame = currentTicks;
    return timeScale;

}
