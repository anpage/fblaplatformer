#include "Player.h"

#define GRAVITY 2.5
#define MOVEMENT 4
#define FRICTION 4

Player::Player(sf::RenderWindow * Application)
{
    app = Application;
    rect = sf::Shape::Rectangle(0, 0, 50, 100, sf::Color(255, 255, 255));
    rect.SetPosition(100, app->GetHeight()-150);
    inAir = true;
    movingLeft = false;
    movingRight = false;
    timer = sf::Clock();
    sf::Image personImg;
    personImg.LoadFromFile("person.png");
    person = sf::Sprite(personImg);
    jumpingPressed = false;
}

Player::~Player()
{
    //dtor
}

void Player::Update(float timeScale)
{

    if (movingRight && velocity.x <= MOVEMENT*3)
    {
        velocity.x = velocity.x + MOVEMENT;
    }

    if (movingLeft && velocity.x >= MOVEMENT*-3)
    {
        velocity.x = velocity.x - MOVEMENT;
    }

    if (!(movingLeft || movingRight))
    {
        if (velocity.x > 0)
        {
            velocity.x = velocity.x - FRICTION * timeScale;
            if (velocity.x < 0)
            {
                velocity.x = 0;
            }
        }
        if (velocity.x < 0)
        {
            velocity.x = velocity.x + FRICTION * timeScale;
            if (velocity.x > 0)
            {
                velocity.x = 0;
            }
        }
    }

    sf::Vector2f pos = rect.GetPosition();
    pos.x = (pos.x + velocity.x * timeScale);
    rect.SetPosition(pos);

    if (inAir == true)
    {
        if (jumping == false)
        {
            velocity.y = velocity.y - GRAVITY * timeScale;
        }
        else
        {
            velocity.y = velocity.y - GRAVITY / 2 * timeScale;
        }
        sf::Vector2f pos = rect.GetPosition();
        pos.y = (pos.y - velocity.y * timeScale);
        rect.SetPosition(pos);
    }

    if (rect.GetPosition().y >= app->GetHeight()-150)
    {
        velocity.y = 0;
        sf::Vector2f pos = rect.GetPosition();
        pos.y = app->GetHeight()-150;
        rect.SetPosition(pos);
        inAir = false;
    }

    if (timer.GetElapsedTime() >= 0.5f)
    {
        jumping = false;
    }

    if (inAir == false && jumpingPressed == true)
    {
        jumping = true;
        timer.Reset();
        inAir = true;
        velocity.y += 30.0f;
    }

    person.SetPosition(rect.GetPosition());
}

void Player::Draw()
{
    //app->Draw(person);
    app->Draw(rect);
}

void Player::SetJumping(bool jump)
{
    jumpingPressed = jump;
    if (jump == false) {jumping = false;}
}

bool Player::Jumping()
{
    return jumpingPressed;
}

void Player::SetMovingRight(bool moving)
{
    movingRight = moving;
}

bool Player::MovingRight()
{
    return movingRight;
}

void Player::SetMovingLeft(bool moving)
{
    movingLeft = moving ;
}

bool Player::MovingLeft()
{
    return movingLeft;
}
