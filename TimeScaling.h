#ifndef TIMESCALING_H
#define TIMESCALING_H

#include <windows.h>

class TimeScaling
{
    public:
        TimeScaling(float targetFPS); //Constructor
        virtual ~TimeScaling(); //Destructor
        float getTimeScale();
    protected:
    private:
        LARGE_INTEGER ticksPerSecond;
        LARGE_INTEGER lastFrame;
        LARGE_INTEGER currentTicks;
        float targetFPS;
};

#endif // TIMESCALING_H
