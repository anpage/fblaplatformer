#ifndef PLAYER_H
#define PLAYER_H
#include <SFML/Graphics.hpp>

class Player
{
    public:
        Player(sf::RenderWindow * Application);
        virtual ~Player();
        void Update(float timeScale);
        void Draw();
        void SetJumping(bool jumping);
        bool Jumping();
        void SetMovingRight(bool moving);
        bool MovingLeft();
        void SetMovingLeft(bool moving);
        bool MovingRight();

    protected:
    private:
        sf::RenderWindow * app;
        sf::Shape rect;
        sf::Sprite person;
        sf::Vector2f velocity;
        sf::Clock timer;
        bool inAir;
        bool jumping;
        bool jumpingPressed;
        bool movingRight;
        bool movingLeft;
        float currentY;
};

#endif // PLAYER_H
